package com.xumak.breakingbad

import android.app.Application
import androidx.room.Room
import com.xumak.breakingbad.common.database.BreakingBadAPI
import com.xumak.breakingbad.common.database.UserDatabase

class UserApplication: Application() {

    companion object{
        lateinit var database: UserDatabase
        lateinit var breakingBadAPI: BreakingBadAPI
    }

    override fun onCreate() {
        super.onCreate()

        // Room
        database = Room.databaseBuilder(
            this,
            UserDatabase::class.java, "UserDatabase")
            .build()

        // Volley
        breakingBadAPI = BreakingBadAPI.getInstance(this)

    }
}