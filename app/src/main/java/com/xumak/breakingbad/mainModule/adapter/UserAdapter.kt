package com.xumak.breakingbad.mainModule.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.xumak.breakingbad.R
import com.xumak.breakingbad.common.entities.UserEntity
import com.xumak.breakingbad.databinding.UserItemBinding

class UserAdapter(private var users: MutableList<UserEntity>, private var listener: OnClickListener):
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    private lateinit var mContext: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = UserItemBinding.bind(view)
        fun setListener(userEntity: UserEntity) {
            with(binding.root){
                setOnClickListener { listener.OnClick(userEntity) }
            }
            binding.fab.setOnClickListener{
                listener.onFavoriteCharacter(userEntity)
            }
        }
    }

    fun setUsers(stores: MutableList<UserEntity>) {
        this.users = stores
        notifyDataSetChanged()
    }

    /*
    ************************************************************************************************************
    * */

    //
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(mContext).inflate(R.layout.user_item, parent, false)
        return ViewHolder(view)
    }
    //
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]
        with(holder) {
            setListener(user)
            with(binding){
                idNameActor.text = user.name
                tvNickName.text = user.nickname
                fab.isChecked = user.isFavorite
                Glide
                    .with(mContext)
                    .load(user.img)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(binding.imgPhoto)
            }

        }
    }
    //
    override fun getItemCount(): Int = this.users.size
}