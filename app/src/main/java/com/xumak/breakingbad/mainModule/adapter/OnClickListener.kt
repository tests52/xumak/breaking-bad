package com.xumak.breakingbad.mainModule.adapter

import com.xumak.breakingbad.common.entities.UserEntity

interface OnClickListener {
    fun OnClick(userEntity: UserEntity)
    fun onFavoriteCharacter(userEntity: UserEntity)
}