package com.xumak.breakingbad.mainModule.model

import android.util.Log
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.xumak.breakingbad.UserApplication
import com.xumak.breakingbad.common.entities.Appearances
import com.xumak.breakingbad.common.entities.BetterCallSaulAppearance
import com.xumak.breakingbad.common.entities.Occupations
import com.xumak.breakingbad.common.entities.UserEntity
import com.xumak.breakingbad.common.utils.Constants
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray


class MainInteractor {
    //

    private fun getUsersApi(callback: (MutableList<UserEntity>)-> Unit ) {

        val url =  Constants.BASE_URL + Constants.BASE_CHARACTERS

        val userList = mutableListOf<UserEntity>()

        val jsonObjectRequest = JsonArrayRequest(Request.Method.GET , url, null , {
                response->
            val jsonResponse = JSONArray(response.toString())
            for (value in 0 until jsonResponse.length()-1) {
                val obj = jsonResponse.getJSONObject(value)
//                Log.i("Response", obj.toString())
                val jsonOccu = JSONArray(obj.get("occupation").toString())
                val occupations: MutableList<Occupations> = mutableListOf<Occupations>()
                for (e in 0 until jsonOccu.length()-1) {
//                    Log.i("Response",  jsonOccu[ocu].toString())
                    occupations.add(
                        Occupations(
                            occupation = jsonOccu[e].toString()
                        ))
                }
                val jsonAppe = JSONArray(obj.get("appearance").toString())
                val appearances: MutableList<Appearances> = mutableListOf<Appearances>()
                for (e in 0 until jsonAppe.length()-1) {
//                    Log.i("Response",  jsonAppe[e].toString())
                    appearances.add(
                        Appearances(
                            appearance = jsonAppe[e] as Int
                        ))
                }
                val jsonBetter = JSONArray(obj.get("appearance").toString())
                val betterCallSaulAppearances: MutableList<BetterCallSaulAppearance> = mutableListOf<BetterCallSaulAppearance>()
                for (e in 0 until jsonAppe.length()-1) {
//                    Log.i("Response",  jsonAppe[e].toString())
                    appearances.add(
                        Appearances(
                            appearance = jsonAppe[e] as Int
                        ))
                }

                val user = UserEntity(
                    charId = obj.getLong("char_id"),
                    name = obj.getString("name"),
                    birthday = obj.getString("birthday"),
                    occupation = occupations,
                    img = obj.getString("img"),
                    status = obj.getString("status"),
                    nickname = obj.getString("nickname"),
                    appearance = appearances,
                    portrayed = obj.getString("portrayed"),
                    category = obj.getString("category"),
                    betterCallSaulAppearance = betterCallSaulAppearances,
                    isFavorite = false
                )

                this.saveUser(user){}
                userList.add(user)

            }
            callback(userList)
            return@JsonArrayRequest
        }, {
            it.printStackTrace()
            callback(userList)
        })

        UserApplication.breakingBadAPI.addToRequestQueue(jsonObjectRequest)
    }

    fun getUsers(callback: (MutableList<UserEntity>)-> Unit ) {
        doAsync {
            val usersList = UserApplication.database.userDao().getAllCharacters()
            uiThread {
                if(usersList.isEmpty()){
                    getUsersApi{
                        response->
                            callback(response)
                    }
                } else {
                    val userListTemp = getUpdatedList(usersList)
                    callback(userListTemp)
                }
            }
        }
    }

    private fun getUpdatedList(usersSelect: MutableList<UserEntity> ): MutableList<UserEntity> {
        val userListTemp = mutableListOf<UserEntity>()

        for (user in usersSelect) {
//            Log.i("Response", user.toString())
            if(user.isFavorite){
                userListTemp.add(user)
            }
        }
        for (user in usersSelect) {
//            Log.i("Response", user.toString())
            if(!user.isFavorite){
                userListTemp.add(user)
            }
        }
        return userListTemp
    }

    private fun saveUser(userEntity: UserEntity, callback: (Long) -> Unit) {
        doAsync {
            val newId = UserApplication.database.userDao().addCharacter(userEntity)
            uiThread {
                callback(newId)
            }
        }
    }

    fun updateUsers(userEntity: UserEntity, callback: (UserEntity) -> Unit) {
        doAsync {
            UserApplication.database.userDao().updateCharacter(userEntity)
            val usersList = UserApplication.database.userDao().getAllCharacters()

            uiThread {
//                Log.i("Response", usersList.toString())
                callback(userEntity)
            }
        }
    }



}