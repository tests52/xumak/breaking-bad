package com.xumak.breakingbad.mainModule

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.xumak.breakingbad.R
import com.xumak.breakingbad.common.entities.UserEntity
import com.xumak.breakingbad.databinding.ActivityMainBinding
import com.xumak.breakingbad.mainModule.adapter.OnClickListener
import com.xumak.breakingbad.mainModule.adapter.UserAdapter
import com.xumak.breakingbad.mainModule.viewModel.MainViewModel
import com.xumak.breakingbad.userModule.UserFragment
import com.xumak.breakingbad.userModule.viewModel.UserViewModel

class MainActivity : AppCompatActivity(), OnClickListener {

    // Controllers
    private lateinit var mBinding: ActivityMainBinding
    private lateinit var mAdapter: UserAdapter
    private lateinit var mGridLayout: GridLayoutManager

    //MVVM
    private lateinit var mMainViewModel: MainViewModel
    private lateinit var mUserViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //
        this.mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(this.mBinding.root)
        //
        this.setupViewModel()
        this.setupRecyclerView()
    }

    private fun setupRecyclerView() {
        this.mAdapter = UserAdapter(mutableListOf(), this)
        this.mGridLayout = GridLayoutManager(this, 2)
        this.mGridLayout.setSpanSizeLookup(object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position % 3 == 2) 2 else 1
            }
        })
        this.mBinding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = mGridLayout
            adapter = mAdapter
        }
    }


    private fun setupViewModel() {
        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        mMainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        mMainViewModel.getUsers().observe(this, {
            users ->
            mAdapter.setUsers(users)
            mMainViewModel.isShowProgress().observe(this) { isShowProgress ->
                mBinding.progressBar.visibility = if (isShowProgress) View.VISIBLE else View.GONE
            }
        })
    }


    private fun launchEditFragment(userEntity: UserEntity = UserEntity()) {

        this.mUserViewModel.setUserSelected(userEntity)

        val fragment = UserFragment()
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        fragmentTransaction.add(R.id.containerMain, fragment)
        fragmentTransaction.commit()
        fragmentTransaction.addToBackStack(null)

    }


    override fun OnClick(userEntity: UserEntity) {
        this.launchEditFragment(userEntity)
    }

    override fun onFavoriteCharacter(userEntity: UserEntity) {
        mMainViewModel.updateUser(userEntity)
    }


}