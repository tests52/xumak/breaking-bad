package com.xumak.breakingbad.mainModule.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.xumak.breakingbad.common.entities.UserEntity
import com.xumak.breakingbad.common.utils.Constants
import com.xumak.breakingbad.mainModule.model.MainInteractor

class MainViewModel: ViewModel() {

    private var userList: MutableList<UserEntity>
    private var interactor: MainInteractor
    private val showProgress: MutableLiveData<Boolean> = MutableLiveData()

    init {
        userList = mutableListOf()
        interactor = MainInteractor()
    }

    private val users: MutableLiveData<MutableList<UserEntity>> by lazy {
        MutableLiveData<MutableList<UserEntity>>().also {
            this.loadUsers()
        }
    }

    fun isShowProgress(): LiveData<Boolean> {
        return showProgress
    }


    fun getUsers(): LiveData<MutableList<UserEntity>> {
        return users;
    }


    private fun loadUsers() {
        showProgress.value = Constants.SHOW
        interactor.getUsers {
            showProgress.value = Constants.HIDE
            users.value = it
            userList = it
        }
    }

    fun updateUser(userEntity: UserEntity) {

        userEntity.isFavorite = !userEntity.isFavorite

        interactor.updateUsers(userEntity) {
            val index = userList.indexOf(userEntity)
            if (index != -1) {
                userList.set(index, userEntity)
                users.value = userList
            }
        }
    }




}