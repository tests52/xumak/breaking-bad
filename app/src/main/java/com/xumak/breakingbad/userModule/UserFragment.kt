package com.xumak.breakingbad.userModule

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.xumak.breakingbad.R
import com.xumak.breakingbad.common.entities.UserEntity
import com.xumak.breakingbad.databinding.FragmentUserBinding
import com.xumak.breakingbad.mainModule.MainActivity
import com.xumak.breakingbad.mainModule.adapter.OnClickListener
import com.xumak.breakingbad.mainModule.viewModel.MainViewModel
import com.xumak.breakingbad.userModule.viewModel.UserViewModel

class UserFragment : Fragment(), OnClickListener {

    private lateinit var mBinding: FragmentUserBinding
    private lateinit var mUserViewModel: UserViewModel
    private lateinit var mMainViewModel: MainViewModel


    private var mActivity: MainActivity? = null
    private var mIsEditMode: Boolean = false
    private lateinit var mUserEntity: UserEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUserViewModel = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        mMainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        this.mBinding = FragmentUserBinding.inflate(inflater, container, false)
        return this.mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // MVVM
        setupViewModel()
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            android.R.id.home -> {
                mActivity?.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onDestroy() {
        mActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        mActivity?.supportActionBar?.title = getString(R.string.app_name)

        mUserViewModel.setResult(Any())

        super.onDestroy()
    }

    private fun setupActionBar(userEntity: UserEntity) {
        mActivity = activity as? MainActivity
        mActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mActivity?.supportActionBar?.title = userEntity.name

    }


    private fun setupViewModel() {
        mUserViewModel.getUserSelected().observe(viewLifecycleOwner){
            mUserEntity = it
            if(it.charId != 0L) {
                mIsEditMode = true
                setUiStore(it)
            }
            setupActionBar(mUserEntity)
        }

        mUserViewModel.getResult().observe(viewLifecycleOwner) { result->
            when(result) {
                is Long ->{
                    mUserEntity.charId = result
                    mUserViewModel.setUserSelected(mUserEntity)
                    mActivity?.onBackPressed()
                }

            }
        }
    }


    private fun setUiStore(userEntity: UserEntity) {
        with(mBinding) {
            var lista = ""
            if(mUserEntity.occupation.isNotEmpty()){
                for (o in mUserEntity.occupation){
                    lista += o.occupation + ","
                }
            } else {
                lista = "No Occupations"
            }
            idNameActor.text = mUserEntity.name
            idOccupations.text = lista
            idStatus.text = mUserEntity.status
            fab.isChecked = mUserEntity.isFavorite

            loadImage(userEntity.img)
        }
    }

    private fun loadImage(url: String) {
        Glide.with(this)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(mBinding.imgPhoto)

    }

    override fun OnClick(userEntity: UserEntity) {
        // No Needed
    }

    override fun onFavoriteCharacter(userEntity: UserEntity) {
        mMainViewModel.updateUser(userEntity)
    }


}