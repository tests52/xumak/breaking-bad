package com.xumak.breakingbad.userModule.model

import com.xumak.breakingbad.UserApplication
import com.xumak.breakingbad.common.entities.UserEntity
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class UserInteractor {

    fun updateUser(userEntity: UserEntity, callback: (UserEntity) -> Unit) {
        doAsync {
            UserApplication.database.userDao().updateCharacter(userEntity)
            uiThread {
                callback(userEntity)
            }
        }
    }


}