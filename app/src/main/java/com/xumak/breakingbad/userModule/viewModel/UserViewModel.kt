package com.xumak.breakingbad.userModule.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.xumak.breakingbad.common.entities.UserEntity
import com.xumak.breakingbad.userModule.model.UserInteractor

class UserViewModel: ViewModel() {

    private val userSelected = MutableLiveData<UserEntity>()
    private val result = MutableLiveData<Any>()
    private val interactor: UserInteractor

    init {
        interactor = UserInteractor()
    }


    fun setUserSelected(userEntity: UserEntity) {
        this.userSelected.value = userEntity
    }

    fun getUserSelected() : LiveData<UserEntity> {
        return this.userSelected
    }

    fun setResult(value: Any) {
        result.value = value
    }

    fun getResult() : LiveData<Any> {
        return result
    }

    fun udpateUser(userEntity: UserEntity) {
        interactor.updateUser(userEntity){
                userUpdated -> result.value = userUpdated
        }
    }




}