package com.xumak.breakingbad.common.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserEntity")
data class UserEntity(
    @PrimaryKey(autoGenerate = true) var charId: Long = 0,
    var name: String = "",
    var birthday: String= "",
    var occupation: List<Occupations> = listOf(),
    var img: String = "",
    var status: String = "",
    var nickname: String = "",
    var appearance: List<Appearances> = listOf(),
    var portrayed: String = "",
    var category: String = "",
    var betterCallSaulAppearance: List<BetterCallSaulAppearance> = listOf(),
    var isFavorite: Boolean = false
    ) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserEntity

        if (charId != other.charId) return false

        return true
    }

    override fun hashCode(): Int {
        return charId.hashCode()
    }
}

