package com.xumak.breakingbad.common.database

import androidx.room.*
import com.xumak.breakingbad.common.entities.UserEntity

@Dao
interface UserDao {
    @Query("SELECT * FROM UserEntity")
    fun getAllCharacters(): MutableList<UserEntity>

    @Insert
    fun addCharacter(userEntity: UserEntity): Long

    @Update
    fun updateCharacter(userEntity: UserEntity)
}


