package com.xumak.breakingbad.common.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.xumak.breakingbad.common.entities.Appearances
import com.xumak.breakingbad.common.entities.Occupations
import com.xumak.breakingbad.common.entities.BetterCallSaulAppearance

class RoomConverters {
    @TypeConverter
    fun OccupationslistToJson(value: List<Occupations>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToListOccupations(value: String) = Gson().fromJson(value, Array<Occupations>::class.java).toList()

    @TypeConverter
    fun listAppearancesToJson(value: List<Appearances>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToListAppearances(value: String) = Gson().fromJson(value, Array<Appearances>::class.java).toList()

    @TypeConverter
    fun listBetterCallSaulAppearanceToJson(value: List<BetterCallSaulAppearance>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToListBetterCallSaulAppearance(value: String) = Gson().fromJson(value, Array<BetterCallSaulAppearance>::class.java).toList()
}