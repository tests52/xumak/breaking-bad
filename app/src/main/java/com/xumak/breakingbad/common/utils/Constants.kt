package com.xumak.breakingbad.common.utils

object Constants {

    // Base API URL
    const val BASE_URL = "https://www.breakingbadapi.com/api"

    // Routes
    const val BASE_CHARACTERS = "/characters?limit=100"

    // FOR SPINNNER
    const val SHOW = true
    const val HIDE = false


}