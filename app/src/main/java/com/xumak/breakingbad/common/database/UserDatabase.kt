package com.xumak.breakingbad.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.xumak.breakingbad.common.entities.UserEntity


@Database(entities = [UserEntity::class], version = 1)
@TypeConverters(RoomConverters::class)
abstract class UserDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}