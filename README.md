# Breaking Bad

The following project is a technical exam for a Senior Android Developer candidate, the following instructions must be followed:

## Code Challenge

The WTA Android Code Challenge consists of an Android application that displays the list of
Breaking Bad Characters using a publicly available REST API on the web. Clicking on a
character opens a simple detail screen with more information about the character.

## Requirements

- [x] It should be possible for the user to browse the list of characters. Users of the app should be
able to toggle a character as a favorite and these selections should be persisted across restarts
of the app. Favorite characters should appear at the top of the list when the list is loaded. The
details screen should show the character’s occupation and status obtained from the API
endpoint.
- [x] The code can be written in the language of your choice and you are free to use open source
libraries as you see fit. We would like to see a bug-free app, with a clean UI, that is written with
a modern architecture. Please write tests where they are appropriate. The app should be
compatible with Android 6.0 or above (API Level 23+)


## Deliverables

- [x] Please submit your source code via a link to Github.
-  In my case, I have a portfolio of projects that I have used for months, so I decided to use GitLab so that they can see the source code, in the same way it is still a public repository

If you want to download the repository, you must follow the git clone command followed by the clone by HTTPS command:

```
git clone https://gitlab.com/tests52/xumak/breaking-bad.git
```

## API Spec:

- [x] /api/characters?limit<LIMIT>&offset=<OFFSET>
- [x] Optional parameters:
    ● limit (integer)
    ● offset (integer)
```
Example: https://www.breakingbadapi.com/api/characters?limit=100
```

You may find the following publicly available API at https://www.breakingbadapi.com

***

## Solution to present:

For the development of the project I propose to use the following libraries with references for the implementation of the solution managing an MVVM architecture which will allow me to have a scalable architecture

- [x] ViewModel: https://developer.android.com/jetpack/androidx/releases/lifecycle
```
    def lifecycle_version = "2.4.0"

    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"
```
- [x] Volley: https://developer.android.com/training/volley?hl=es-419
```
    implementation 'com.android.volley:volley:1.1.1'
```
- [x] GSON: https://github.com/google/gson
```
    implementation 'com.google.code.gson:gson:2.8.9'
```
- [x] Room: https://developer.android.com/training/data-storage/room
```
    def room_version = "2.3.0"

    implementation "androidx.room:room-runtime:$room_version"
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    kapt "androidx.room:room-compiler:$room_version"
```
- [x] Anko: https://github.com/Kotlin/anko
```
    def anko_version= "0.10.8"

    implementation "org.jetbrains.anko:anko-commons:$anko_version"
```
- [x] Glide https://github.com/bumptech/glide
```
    implementation 'com.github.bumptech.glide:glide:4.11.0'
    kapt 'com.github.bumptech.glide:compiler:4.11.0'

```
- [x] Material Design Reference: https://material.io/components/lists


The following permission is also added that will allow the application to access the images from outside the internet

```
    <uses-permission android:name="android.permission.INTERNET" />
```

Important note, the following attribute must be added in the activity .MainActivity

```
    <activity android:name=".MainActivity" android:exported="true">
```

***

## Solution to present:


According to the specifications of the code challenge, the following design must be followed, however I decided to rely on the cards found on the official site The Breaking Bad API, as a simple reference with a clean design, to show the list of actors

Another important detail is that the project was carried out with Kotlin for the development and management of the interface state

![image](/markdown/kotlin.jpg)


***

## Code Challenge Exam Image
![image](/markdown/foto4.png) 


***

## Reference art for GRID design

![image](/markdown/foto3.png)

***

## My Code Challenge solution

![image](/markdown/foto1.png)
![image](/markdown/foto2.png)



## References

- The Breaking Bad:  https://www.breakingbadapi.com/documentation
- Clean Code Architecture: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html
- Reference design, to make my own design: https://www.breakingbadapi.com/
- Material Design: https://material.io/blog/start-building-with-material-you
- Tutorials: https://material.io/resources/tutorials#android-java
- Tutorials: https://codelabs.developers.google.com/codelabs/mdc-102-java#0
- Tutorials: https://www.udemy.com/course/kotlin-intensivo/